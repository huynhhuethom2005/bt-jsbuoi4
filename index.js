/* 
BT1:
- Tạo ra 3 biến lấy giá trị người dùng nhập vào
- Viết câu lệnh điều kiện so sánh để in ra số thứ tự tăng dần
*/

function bt1() {
    let soNguyen1 = document.getElementById("soNguyen1").value * 1,
        soNguyen2 = document.getElementById("soNguyen2").value * 1,
        soNguyen3 = document.getElementById("soNguyen3").value * 1,
        resultBt1 = document.getElementById("resultBt1");

    if (soNguyen1 < soNguyen2 && soNguyen2 < soNguyen3) {
        resultBt1.innerHTML = `${soNguyen1}, ${soNguyen2}, ${soNguyen3}`
    } else if (soNguyen1 < soNguyen3 && soNguyen3 < soNguyen2) {
        resultBt1.innerHTML = `${soNguyen1}, ${soNguyen3}, ${soNguyen2}`
    } else if (soNguyen2 < soNguyen1 && soNguyen1 < soNguyen3) {
        resultBt1.innerHTML = `${soNguyen2}, ${soNguyen1}, ${soNguyen3}`
    } else if (soNguyen2 < soNguyen3 && soNguyen3 < soNguyen1) {
        resultBt1.innerHTML = `${soNguyen2}, ${soNguyen3}, ${soNguyen1}`
    } else if (soNguyen3 < soNguyen1 && soNguyen1 < soNguyen2) {
        resultBt1.innerHTML = `${soNguyen3}, ${soNguyen1}, ${soNguyen2}`
    } else if (soNguyen3 < soNguyen2 && soNguyen2 < soNguyen1) {
        resultBt1.innerHTML = `${soNguyen3}, ${soNguyen2}, ${soNguyen1}`
    }
}

/*
BT2:
- Nhận giá trị nhập vào từ người dùng
- Sau đó so sánh điều kiện để gắn biến chào hỏi phù hợp
*/

function bt2() {
    let thanhVien = document.getElementById("thanhVien").value,
        resultBt2 = document.getElementById("resultBt2")
    if (thanhVien == "Bố" || thanhVien == "bố") {
        resultBt2.innerHTML = `Con chào bố!`
    } else if (thanhVien == "Mẹ" || thanhVien == "mẹ") {
        resultBt2.innerHTML = `Con chào mẹ!`
    } else if (thanhVien == "anh Hai" || thanhVien == "anh hai") {
        resultBt2.innerHTML = `Em chào anh Hai!`
    } else if (thanhVien == "Em gái" || thanhVien == "em gái") {
        resultBt2.innerHTML = `Chào em gái!`
    }
}

/* 
BT3:
- Tạo biến lấy giá trị người dùng nhập vào
- So sánh điều kiện nếu số chia hết cho 2 thì cộng chuỗi vào soChanBt3
- Nếu không chia hết cho 2 thì cộng chuỗi vào soLeBt3
*/

function bt3() {
    let so1 = document.getElementById("so1").value * 1
    let so2 = document.getElementById("so2").value * 1
    let so3 = document.getElementById("so3").value * 1
    let resultSoChan = 0
    let resultSoLe = 0

    if (so1 % 2 == 0) {
        resultSoChan += 1
    } else if (so1 % 2 != 0) {
        resultSoLe += 1
    }
    if (so2 % 2 == 0) {
        resultSoChan += 1
    } else if (so2 % 2 != 0) {
        resultSoLe += 1
    }
    if (so3 % 2 == 0) {
        resultSoChan += 1
    } else if (so3 % 2 != 0) {
        resultSoLe += 1
    }

    document.getElementById("soChanBt3").innerHTML = `Số chẵn là: ${resultSoChan}`
    document.getElementById("soLeBt3").innerHTML = `Số lẽ là: ${resultSoLe}`
}

/*
BT4:
- Tạo biến lấy giá trị 3 cạnh
- So sánh theo điều kiện
• a = 3, b=3 c=3 => Tam giác đều
• a=2, b=2, c=1 => Tam giác cân
• a = 3, b = 4, c=5 => Tam giác vuông
*/

function bt4() {
    let canh1 = document.getElementById("canh1").value * 1
    let canh2 = document.getElementById("canh2").value * 1
    let canh3 = document.getElementById("canh3").value * 1

    if (canh1 == canh2 && canh1 == canh3) {
        document.getElementById("resultBt4").innerHTML = `Tam giác đều`
    } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
        document.getElementById("resultBt4").innerHTML = `Tam giác cân`
    } else if (
        Math.pow(canh1, 2) + Math.pow(canh2, 2) == Math.pow(canh3, 2) ||
        Math.pow(canh1, 2) + Math.pow(canh3, 2) == Math.pow(canh2, 2) ||
        Math.pow(canh3, 2) + Math.pow(canh2, 2) == Math.pow(canh1, 2)
    ) {
        document.getElementById("resultBt4").innerHTML = `Tam giác vuông`
    } else {
        document.getElementById("resultBt4").innerHTML = `Tam giác thường`
    }
}
